package dam.androidempar.u3t2ImplicitIntents;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ShareCompat;


// If part of your app depends on knowing whether the call to startActivity()
// can succeed, such as showing a UI, add an element to the <queries> element
// of your app's manifest. Typically, this new element is an <intent> element.

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    public static final String IMPLICIT_INTENTS = "ImplicitIntents";
    private EditText etUri, etLocation, etText;
    // TODO Ex1 ZOOM
    private EditText etZoomValue;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setUI();
    }

    private void setUI() {
        Button btOpenUri, btOpenLocation, btShareText;


        etUri = findViewById(R.id.etUri);
        etLocation = findViewById(R.id.etLocation);
        etText = findViewById(R.id.etText);

        btOpenUri = findViewById(R.id.btOpenUri);
        btOpenLocation = findViewById(R.id.btOpenLocation);
        btShareText = findViewById(R.id.btShareText);

        btOpenUri.setOnClickListener(this);
        btOpenLocation.setOnClickListener(this);
        btShareText.setOnClickListener(this);

        // TODO Ex1 ZOOM
        etZoomValue = findViewById(R.id.etZoomValue);

        // TODO Ex3 5 MORE INTENTS
        findViewById(R.id.btMoreIntents).setOnClickListener(this);
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btOpenUri:
                openWebsite(etUri.getText().toString());
                break;
            case R.id.btOpenLocation:
                // TODO Ex1 ZOOM
                try {
                    // TODO Ex1 ZOOM
                    openLocation(etLocation.getText().toString(), Integer.parseInt(etZoomValue.getText().toString()));
                    etZoomValue.setError(null);
                } catch (Exception e) {
                    Log.d("error", e.getMessage());
                    etZoomValue.setError(e.getMessage());
                }
                break;
            case R.id.btShareText:
                shareText(etText.getText().toString());
                break;
            // TODO Ex3 5 MORE INTENTS
            case R.id.btMoreIntents:
                openMoreIntentsActivity();
                break;
        }
    }



    private void openWebsite(String urlText) {
        // Parse the URI and create the intent.
        Uri webpage = Uri.parse(urlText);
        Intent intent = new Intent(Intent.ACTION_VIEW, webpage);

        // Find an activity to hand the intent and start that activity.
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        } else {
            Log.d(IMPLICIT_INTENTS, "openWebsite: Can't handle this intent!");
        }
    }

    private void openLocation(String location, int zoom) throws Exception {
        //You can use:
        // - a geo URI with latitude and longitude,
        // - or use a query string for a general location.
        // We've used the latter.

        // Parse the location and create the intent.
        //Uri addressUri = Uri.parse("geo:0,0?q=" + location);
        // TODO Ex1 ZOOM:
        if (zoom < 1 || zoom > 23) throw new Exception(zoom + " is not a valid value for Map zoom: 1-23");
        Uri addressUri = Uri.parse("geo:0,0?z="+ zoom +"&q=" + location);
        //Uri addressUri = Uri.parse("geo:38.70545,-0.47432?z="+ zoom);

        Intent intent = new Intent(Intent.ACTION_VIEW, addressUri);

        // Find an activity to handle the intent, and start that activity.
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        } else {
            Log.d("ImplicitIntents", "openLocation: Can't handle this intent!");
        }
    }

    private void shareText(String text) {
        new ShareCompat.IntentBuilder(this)
                .setType("text/plain") // The MIME type of the item to be shared.
                .setText(text)         // actual text to share.
                                       // Equivalent to: intent.putExtra(Intent.EXTRA_TEXT, text)
                .startChooser();       // Start a chooser activity for the current share intent.
    }

    // TODO Ex3 5 MORE INTENTS
    private void openMoreIntentsActivity() {
        startActivity(new Intent(this, MoreIntentsActivity.class));
    }

}