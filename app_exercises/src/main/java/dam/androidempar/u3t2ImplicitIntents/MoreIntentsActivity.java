package dam.androidempar.u3t2ImplicitIntents;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.AlarmClock;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.sql.Date;
import java.util.Calendar;

public class MoreIntentsActivity extends AppCompatActivity implements View.OnClickListener {
    EditText etPhoneNumber, etSmsMessage, etTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_more_intents);

        setUI();
    }

    private void setUI() {
        etPhoneNumber = findViewById(R.id.etPhoneNumber);
        etSmsMessage = findViewById(R.id.etSmsMessage);
        etTime = findViewById(R.id.etTime);

        findViewById(R.id.ivOpenCamera).setOnClickListener(this);
        findViewById(R.id.ivPhoneCall).setOnClickListener(this);
        findViewById(R.id.ivSendSms).setOnClickListener(this);
        findViewById(R.id.ivSetAlarm).setOnClickListener(this);

    }


    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivOpenCamera:
                takePicture();
                break;
            case R.id.ivPhoneCall:
                dialPhone(etPhoneNumber.getText().toString());
                break;
            case R.id.ivSendSms:
                sendSms(etPhoneNumber.getText().toString(), etSmsMessage.getText().toString());
                break;
            case R.id.ivSetAlarm:
                etTime.setError(null);
                try {
                    setAlarm(etSmsMessage.getText().toString());
                } catch (Exception ex) {
                    etTime.setError("Format--> hh:mm");
                }

                break;
        }
    }

    private void takePicture() {
        Intent intent = new Intent(MediaStore.INTENT_ACTION_STILL_IMAGE_CAMERA);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        } else
            Toast.makeText(this, "Camera app is not available", Toast.LENGTH_LONG).show();
    }

    private void dialPhone(String number) {
        Intent callIntent = new Intent(Intent.ACTION_DIAL);

        callIntent.setData(Uri.parse("tel:" + number));

        if (callIntent.resolveActivity(getPackageManager()) != null) {
            startActivity(callIntent);
        } else
            Toast.makeText(this, "Phone app is not available", Toast.LENGTH_LONG).show();

    }

    private void sendSms(String phoneNumber, String smsMessage) {
        Intent callIntent = new Intent(Intent.ACTION_SENDTO);
        callIntent.setData(Uri.parse("smsto:" + phoneNumber));
        callIntent.putExtra("sms_body", smsMessage);

        if (callIntent.resolveActivity(getPackageManager()) != null) {
            startActivity(callIntent);
        } else
            Toast.makeText(this, "Sms app is not available", Toast.LENGTH_LONG).show();
    }

    private void setAlarm(String message) throws Exception{
        if (!etTime.getText().toString().matches("([0-2][0-9]):([0-5][0-9])"))
            throw new Exception();

        int hour = Integer.parseInt(etTime.getText().toString().substring(0, 2));
        int minutes = Integer.parseInt(etTime.getText().toString().substring(3, 5));

        Intent callIntent = new Intent(AlarmClock.ACTION_SET_ALARM)
                .putExtra(AlarmClock.EXTRA_MESSAGE, message)
                .putExtra(AlarmClock.EXTRA_HOUR, hour)
                .putExtra(AlarmClock.EXTRA_MINUTES, minutes);

        if (callIntent.resolveActivity(getPackageManager()) != null) {
            startActivity(callIntent);
        } else
            Toast.makeText(this, "Alarm app is not available", Toast.LENGTH_LONG).show();

    }
}