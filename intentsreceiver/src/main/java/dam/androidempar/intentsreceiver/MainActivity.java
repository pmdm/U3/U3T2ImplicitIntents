package dam.androidempar.intentsreceiver;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;




// If part of your app depends on knowing whether the call to startActivity()
// can succeed, such as showing a UI, add an element to the <queries> element
// of your app's manifest. Typically, this new element is an <intent> element.


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Intent intent = getIntent();
        String extraText = "INTENT: FROM LAUNCHER";

        TextView textView = findViewById(R.id.txtUri);

        if (intent.getExtras() != null) {
            extraText = intent.getExtras().getString(Intent.EXTRA_TEXT);
            Log.i("TAG", "Text data from Intent: " + extraText);
        } else {
            Log.i("TAG", extraText);
        }
        textView.setText(extraText);
    }
}